import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:iExchange_it/src/models/user.dart';
import 'package:iExchange_it/src/repository/settings_repository.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:iExchange_it/src/repository/user_repository.dart' as userRepo;
import 'package:http/http.dart' as http;

class UserController extends ControllerMVC {

  User user;

  User currentUser = User();
  bool isLoading = false;
  TextEditingController passwordController;

  GlobalKey<ScaffoldState> scaffoldKey;
  GlobalKey<FormState> formKey;

  GoogleSignIn _googleSignIn = GoogleSignIn(scopes: [
    'email',
  ],);

  UserController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    this.formKey = new GlobalKey<FormState>();
    passwordController = TextEditingController();
  }

  void login() async {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      setState((){isLoading = true;});
      userRepo.login(currentUser).then((_user) {
        setState((){isLoading = false;});
        if (_user.apiToken != null && _user.apiToken.toString().length > 0) {
          Navigator.of(this.scaffoldKey.currentContext).pushNamedAndRemoveUntil(
              "/Pages", (route) => false,
              arguments: 0);
        } else {
          scaffoldKey.currentState
              .showSnackBar(SnackBar(content: Text("Wrong email or password")));
        }
      }, onError: (e) {
        print(e);
        setState((){isLoading = false;});
        scaffoldKey.currentState.showSnackBar(
            SnackBar(content: Text("Verify your internet connection")));
      });
    }
  }

  void register() async {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      setState((){isLoading = true;});
      userRepo.register(currentUser).then((_user) {
        setState((){isLoading = false;});
        if (_user.apiToken != null && _user.apiToken.toString().length > 0) {
          Navigator.of(this.scaffoldKey.currentContext).pushNamedAndRemoveUntil(
              "/Pages", (route) => false,
              arguments: 0);
        } else {
          scaffoldKey.currentState
              .showSnackBar(SnackBar(content: Text("Wrong email or password")));
        }
      }, onError: (e) {
        print(e);
        setState((){isLoading = false;});
        scaffoldKey.currentState.showSnackBar(
            SnackBar(content: Text("Verify your internet connection")));
      });
    }
  }

  void getCurrentUser() async {
    userRepo.getCurrentUser().then((_user) {
      setState((){
        this.currentUser = _user;
      });
    });
  }

  void signUpWithFacebook() async {
    setState((){ isLoading = true; });
    if(await isInternetConnection()) {

      final facebookLogin = FacebookLogin();
      final result = await facebookLogin.logIn(['email']);

      switch (result.status) {
        case FacebookLoginStatus.loggedIn:
          final token = result.accessToken.token;
          User _user = await handleFbLogin(token);
          socialLogin(_user, "facebook");
          break;
        case FacebookLoginStatus.cancelledByUser:
          setState((){isLoading = false;});
          break;
        case FacebookLoginStatus.error:
          setState((){isLoading = false;});
          scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text('Unable to login now'),
          ));
          break;
      }
    } else {
      setState((){ isLoading = false; });
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Verify your internet connection")));
    }
  }

  void signUpWithGoogle() async {
    setState((){ isLoading = true; });
    if(await isInternetConnection()) {

      try {
        if(await _googleSignIn.isSignedIn())
        {
          await _googleSignIn.signOut();
        }
        GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
        if(googleSignInAccount != null) {
          debugPrint("Google Signin not null ");
          String email = googleSignInAccount.email;
          String name = googleSignInAccount.displayName;
          User _googleUser = User();
          _googleUser.email = email;
          _googleUser.name = name;

          socialLogin(_googleUser, "google");

        } else {
          setState((){isLoading = false;});
        }
      } catch (error) {
        setState((){isLoading = false;});
        print("Error Google Signin");
        print(error);
      }

    } else {
      setState((){ isLoading = true; });
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Verify your internet connection")));
    }
  }



  void socialLogin(User _user, String method) async {
    userRepo.socialLogin(_user, method).then((_user) {
      setState((){isLoading = false;});
      if(_user.alreadyExists) {
        scaffoldKey.currentState
            .showSnackBar(
            SnackBar(content: Text("Email already registered")));
      } else {
        if (_user.apiToken != null && _user.apiToken
            .toString()
            .length > 0) {

          Navigator.of(this.scaffoldKey.currentContext).pushNamedAndRemoveUntil(
              "/Pages", (route) => false,
              arguments: 0);
        } else {
          scaffoldKey.currentState
              .showSnackBar(
              SnackBar(content: Text("Wrong email or password")));
        }
      }
    }, onError: (e) {
      print(e);
      setState((){isLoading = false;});
      scaffoldKey.currentState.showSnackBar(
          SnackBar(content: Text("Verify your internet connection")));
    });
  }

  Future<User> handleFbLogin(String token) async {
    final graphResponse = await http.get(
        Uri.parse('https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${token}'));
    //final profile = JSON.decode(graphResponse.body);
    var body = jsonDecode(graphResponse.body);
    User _user = User();
    _user.name = body['name'];
    _user.email = body['email'];
    print("Facebook profile is: "+ body.toString());
    return _user;
    // setState((){ isLoading = true; });

  }

}