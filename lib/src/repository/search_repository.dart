import 'dart:convert';
import 'dart:io';
import 'package:global_configuration/global_configuration.dart';
import 'package:iExchange_it/src/helper/helper.dart';
import 'package:iExchange_it/src/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart' as parser;
import 'package:iExchange_it/src/models/product.dart';

import '../models/attribute.dart';

Future<Stream<Product>> searchProductWithoutFilters(String keyword, String lat, String long, String range) async {
  final String _keyword = '?keyword=$keyword';
  final String _lat = '&latitude=$lat';
  final String _long = '&longitude=$long';
  final String _range= '&range=$range';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}search$_keyword$_lat$_long$_range';
  final client = new http.Client();
  final streamedRest =await client.send(http.Request('post', Uri.parse(url)));
  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getProductsData(data))
      .expand((element) {
        return element;
      })
      .map((data) {
    var d = data;
    return Product.fromJson(data);
  });

}

Future<Stream<Product>> searchProductWithFilters(var keyword, var postCode, var type, var location,
    var categoryId, var subCatId, var childCat, var condition, var minPrice, var maxPrice, var duration,
    String lat, String long, String range,
    { List<Attribute> attrs : const <Attribute>[] } ) async {

  final String _keyword = '?keyword=$keyword';
  final String _postCode = '&postcode=$postCode';
  final String _type = '&type=$type';
  final String _location = '&location=$location';
  final String _catId = '&category_id=$categoryId';
  final String _subCat = '&sub_category_id=$subCatId';
  final String _childCat = '&child_category_id=$childCat';
  final String _condition = '&condition=$condition';
  final String _minPrice = '&minprice=$minPrice';
  final String _maxPrice = '&maxprice=$maxPrice';
  final String _duration = '&duration=$duration';

  final String _lat = '&latitude=$lat';
  final String _long = '&longitude=$long';
  final String _range= '&range=$range';

  Map<String, String> bodyMap = Map<String, String>();
  bodyMap['keyword'] = keyword.toString();
  bodyMap['postcode'] = postCode.toString();
  bodyMap['type'] = type.toString();
  bodyMap['location'] = location.toString();
  bodyMap['category_id'] = categoryId.toString();
  bodyMap['sub_category_id'] = subCatId.toString();
  bodyMap['child_category_id'] = childCat.toString();
  bodyMap['condition'] = condition.toString();
  bodyMap['minprice'] = minPrice.toString();
  bodyMap['maxprice'] = maxPrice.toString();
  bodyMap['duration'] = duration.toString();
  bodyMap['latitude'] = lat.toString();
  bodyMap['longitude'] = long.toString();
  bodyMap['range'] = range.toString();

  // bodyMap['keyword'] = "";
  // bodyMap['postcode'] = "";
  // bodyMap['type'] = "";
  // bodyMap['location'] = "";
  // bodyMap['category_id'] = "";
  // bodyMap['sub_category_id'] = "";
  // bodyMap['child_category_id'] = "";
  // bodyMap['condition'] = "";
  // bodyMap['minprice'] = "";
  // bodyMap['maxprice'] = "";
  // bodyMap['duration'] = "all";
  // bodyMap['latitude'] = "";
  // bodyMap['longitude'] = "";
  // bodyMap['range'] = "";

  String url = '${GlobalConfiguration().getValue('api_base_url')}detail/search';

  if(attrs != null && attrs.isNotEmpty) {
    attrs.forEach((element) {
      bodyMap['attrs[${element.id}]'] = element.valueToSend.toString();
    });
  }

  // final client = new http.Client();
  //
  // var req = http.Request('post', Uri.parse(url));
  // req.bodyFields = bodyMap;

  http.MultipartRequest request = http.MultipartRequest('post', Uri.parse(url));
  request.fields.addAll(bodyMap);
  var response = await request.send();

  // final streamedRest =await client.send(req);

  return response.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) {
        print(data);
        return Helper.getProductsData(data);})
      .expand((element) => element)
      .map((data) {
    var d = data;
    return Product.fromJson(data);
  });

}